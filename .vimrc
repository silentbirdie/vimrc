" Turn on syntax highlighting
syntax on

" Show line numbers
set number

" Show file stats
set ruler

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Status bar
set laststatus=2

" Last line
set showmode
set showcmd

" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
